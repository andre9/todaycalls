import React, { Component } from 'react';
import './App.css';

import NextCall from './NextCall';
import CallList from './CallList';
import AddCall from './AddCall';

class App extends Component {
  state = {
    calls: [{
      name: 'Mark Bergman',
      phone: '00420 111 222 333',
      time: 10*60*60+40*60,
    }]
  }

  onAddCall = (call) => {
    this.setState(prevState => ({
      calls: [...prevState.calls, call]
    }));
  }

  onDeleteCall = (index) => {
    this.setState(prevState => ({
      calls: prevState.calls.filter((_, i) => i !== index)
    }));
  }

  render() {
    return (
      <main role="main" class="container mt-3">
        <div class="row">
          <div class="col-lg-5 mb-3">
            <NextCall
              calls={this.state.calls}
            />
          </div>
          <div class="col-lg-7 mb-3">
            <AddCall
              onAddCall={this.onAddCall}
            />
          </div>
          <div class="col-lg-7 offset-lg-5 mb-3">
            <CallList
              calls={this.state.calls}
              onDeleteCall={this.onDeleteCall}
            />
          </div>
        </div>
      </main>
    );
  }
}

export default App;
