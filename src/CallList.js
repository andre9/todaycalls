import React, { Component } from 'react';
import classNames from 'classnames';

import { currentTime, formatTime } from './utils/time';

class CallList extends Component {
  state = {
    filter: 'all',
    sortField: 'name',
    sortOrder: 'asc',
  }

  handleDeleteClick = (index, event) => {
    this.props.onDeleteCall(index);
    event.preventDefault();
  }

  handleFilterClick = (filter, event) => {
    this.setState({
      filter: filter
    });

    event.preventDefault();
  }

  handleSortClick = (field, event) => {
    const orderSwap = { asc: 'desc', desc: 'asc'};

    let order = this.state.sortOrder;
    if (this.state.sortField === field) {
      order = orderSwap[order];
    }

    this.setState({
      sortField: field,
      sortOrder: order,
    });

    event.preventDefault();
  }

  render() {
    const currTime = currentTime();

    return (
      <div class="card">
        <div class="card-header">
          Call List
        </div>
        <div class="card-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">
                  <SortIcon
                    sortField={this.state.sortField}
                    sortOrder={this.state.sortOrder}
                    field='name'
                    onClick={this.handleSortClick}
                  />
                  Name
                </th>
                <th scope="col">Phone Number</th>
                <th scope="col">
                  <SortIcon
                    sortField={this.state.sortField}
                    sortOrder={this.state.sortOrder}
                    field='time'
                    onClick={this.handleSortClick}
                  />
                  Time
                </th>
                <th scope="col">&nbsp;</th>
                <th scope="col">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              {this.props.calls.filter(call => {
                switch (this.state.filter) {
                  case 'next':
                    return call.time > currTime;
                  case 'finished':
                    return call.time <= currTime;
                  case 'all':
                  default:
                    return true;
                }
              }).sort((a, b) => {
                let cmp = a[this.state.sortField] > b[this.state.sortField];

                if (this.state.sortField === 'name') {
                  cmp = a.name.toLowerCase().localeCompare(b.name.toLowerCase());
                }

                if (this.state.sortOrder === 'desc') {
                  return !cmp;
                }

                return cmp;
              }).map((call, index) =>
                <tr>
                  <td>{call.name}</td>
                  <td>{call.phone}</td>
                  <td>{formatTime(call.time)}</td>
                  <td><button type="button" class="btn btn-danger badge" onClick={this.handleDeleteClick.bind(this, index)}>delete</button></td>
                  <td>
                    <input type="checkbox" checked={call.time < currTime} disabled aria-label="time passed" />
                  </td>
                </tr>
              )}
            </tbody>
          </table>
          <div class="text-center">
            <div class="btn-group">
              <FilterButton
                filter={this.state.filter}
                label='All'
                value='all'
                onClick={this.handleFilterClick}
              />
              <FilterButton
                filter={this.state.filter}
                label='Next'
                value='next'
                onClick={this.handleFilterClick}
              />
              <FilterButton
                filter={this.state.filter}
                label='Finished'
                value='finished'
                onClick={this.handleFilterClick}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function FilterButton(props) {
  const btnClass = classNames({
    'btn': true,
    'btn-secondary': true,
    'active': props.filter === props.value,
  });

  return (
    <button className={btnClass} onClick={props.onClick.bind(null, props.value)}>
      {props.label}
    </button>
  );
}

function SortIcon(props) {
  const iconClass = classNames({
    'fas': true,
    'fa-sort': props.field !== props.sortField,
    'fa-sort-up': props.field === props.sortField && props.sortOrder === 'asc',
    'fa-sort-down': props.field === props.sortField && props.sortOrder === 'desc',
  });

  return (
    <button className={iconClass} onClick={props.onClick.bind(null, props.field)}></button>
  );
}

export default CallList;
