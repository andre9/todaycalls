import moment from 'moment';

export function currentTime() {
  let currTime = moment();
  return moment.utc(0).set('hour', currTime.get('hour')).set('minute', currTime.get('minute')).unix();
};

export function formatTime(time) {
  return moment.unix(time).utc().format("HH:mm");
}

export function parseTime(time) {
  let parsedTime = moment.utc(time, "HH:mm");
  return moment.utc(0).set('hour', parsedTime.get('hour')).set('minute', parsedTime.get('minute')).unix();
}
