import React, { Component } from 'react';

import { currentTime, formatTime } from './utils/time';

class NextCall extends Component {
  render() {
    let currTime = currentTime();

    const nextCall = this.props.calls.sort((a, b) => {
      return a.time - b.time;
    }).find(element => {
      return element.time > currTime;
    });

    return (
      <div class="card">
        <div class="card-header">
          Next Call
        </div>
        <div class="card-body">
          <table class="table table-bordered table-sm mb-0">
            {nextCall &&
              <tr>
                <td>{nextCall.name}</td>
                <td>{nextCall.phone}</td>
                <td>{formatTime(nextCall.time)}</td>
              </tr>
            }
          </table>
        </div>
      </div>
    );
  }
}

export default NextCall;
