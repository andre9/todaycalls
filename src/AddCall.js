import React, { Component } from 'react';

import { parseTime } from './utils/time';

class AddCall extends Component {
  state = {
    name: '',
    phone: '',
    time: '',
  }

  handleAddClick = (event) => {
    const time = parseTime(this.state.time);

    this.props.onAddCall({
      ...this.state,
      time
    });

    event.preventDefault();
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div class="card">
        <div class="card-header">
          Add Call
        </div>
        <div class="card-body">
          <form>
            <div class="form-group mb-0">
              <div class="form-row">
                <div class="col-4">
                  <input type="text" class="form-control" placeholder="Name" name="name" value={this.state.name} onChange={this.handleInputChange}/>
                </div>
                <div class="col-4">
                  <input type="text" class="form-control" placeholder="Phone number" name="phone" value={this.state.phone} onChange={this.handleInputChange}/>
                </div>
                <div class="col-2">
                  <input type="text" class="form-control" placeholder="Time" name="time" value={this.state.time} onChange={this.handleInputChange}/>
                </div>
                <div class="col-2">
                  <button class="btn btn-primary" onClick={this.handleAddClick}>Add</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default AddCall;
